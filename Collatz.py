#!/usr/bin/env python3
# ----------
# Collatz.py
# ----------
# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# -------
# imports
# -------
from typing import IO, List
from Meta_Cache import cycle_bucket

# ------------
# collatz_read
# ------------
def collatz_read(s: str) -> List[int] :
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------
def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    # Asserts Precondiitions of the problem
    assert i > 0
    assert j > 0
    assert i < 1000000
    assert j < 1000000

    # Makes sure i <= j for code below
    if i > j:
        i, j = j, i

    # Stores greatest cycle length for size 1000 buckets
    cycle_list = cycle_bucket
    # Lazy cache
    cycle_dict = {
        1: 1,
        2: 2,
        4: 3,
        8: 4,
        16: 5,
        32: 6,
        64: 7,
        128: 8,
        256: 9,
        512: 10,
        1024: 11,
        2048: 12,
        4096: 13,
        8192: 14,
        16384: 15,
        32768: 16,
        65536: 17,
        131072: 18,
        262144: 19,
        524288: 20,
    }

    # Keeps track of max cycle value
    max_cycle = 0
    # Keeps track of current number
    number = i

    # Loops till cycle length found for values between i and j
    while number <= j:
        # Checks if a range of numbers encompasses an entire bucket
        if number % 1000 == 0 and number + 1000 <= j:
            index = number // 1000
            curr_cycle = cycle_list[index]

            if curr_cycle > max_cycle:
                max_cycle = curr_cycle

            number += 1000
        # Calculates cycle length for an individual number
        else:
            # Stores values found during collatz operation
            prev_val = []

            curr_val = number
            cycle_length = 0

            # Performs Collatz
            while cycle_dict.get(curr_val) is None:
                prev_val.append(curr_val)
                if curr_val % 2 == 1:
                    curr_val = curr_val * 3 + 1
                else:
                    curr_val //= 2
                cycle_length += 1

            # Adds the number and additional values found into cache
            prev_size = len(prev_val)
            found_cycle_len = cycle_dict[curr_val]

            curr_cycle = found_cycle_len + cycle_length
            if curr_cycle > max_cycle:
                max_cycle = curr_cycle

            for index in range(prev_size):
                cycle_dict[prev_val[index]] = curr_cycle
                curr_cycle -= 1

            number += 1

    return max_cycle


# -------------
# collatz_print
# -------------
def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------
def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
